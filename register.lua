local S = minetest.get_translator("unified_inventory")
local NS = function(s) return s end
local F = minetest.formspec_escape
local ui = unified_inventory

minetest.register_privilege("creative", {
	description = S("Can use the creative inventory"),
	give_to_singleplayer = false,
})

minetest.register_privilege("ui_full", {
	description = S("Forces Unified Inventory to be displayed in Full mode if Lite mode is configured globally"),
	give_to_singleplayer = false,
})

local trash = minetest.create_detached_inventory("trash", {
	allow_put = function(inv, listname, index, stack, player)
        local name_stack = stack:get_name()
		if minetest.get_item_group(name_stack, "bag") > 0 or name_stack:find("dinosaur") or name_stack:find("pchest") then
			return 0
		else
			return stack:get_count()
		end
	end,
	on_put = function(inv, listname, index, stack, player)
		local player_name = player:get_player_name()
		local count_stack = stack:get_count()
		local name_stack = stack:get_name()
        local wear_stack = stack:get_wear() or 0
		local pos = player:getpos()
		local meta = minetest.get_meta(pos)
		
		local trash = inv:set_stack(listname, index, nil)
		if trash then -- Megaaa, then we're sure
			minetest.sound_play("trash", {to_player=player_name, gain = 1.0})
			minetest.log(player_name.. " trashed ".. count_stack.. " ".. name_stack) -- Example, if I trash 99banana : "pseudonyme trashed 99 ethereal:banana"
			-- Extra new (used for undo item trashed)
			-- Set string&int with meta.
			player:set_attribute("trashed_before", name_stack) -- name
			player:set_attribute("trashed_count", count_stack) -- count
			player:set_attribute("trashed_wear", wear_stack)
		end
        --minetest.chat_send_all(name_stack)
        --minetest.chat_send_all(count_stack)
	end,
})
trash:set_size("main", 1)

ui.register_button("craft", {
	type = "image",
	image = "ui_craft_icon.png",
	tooltip = S("Crafting Grid")
})

ui.register_button("craftguide", {
	type = "image",
	image = "ui_craftguide_icon.png",
	tooltip = S("Crafting Guide")
})

ui.register_button("home_gui_set", {
	type = "image",
	image = "ui_sethome_icon.png",
	tooltip = S("Set home position"),
	hide_lite=true,
	action = function(player)
		local player_name = player:get_player_name()
		if minetest.check_player_privs(player_name, {home=true}) then
			ui.set_home(player, player:get_pos())
			local home = ui.home_pos[player_name]
			if home ~= nil then
				minetest.sound_play("dingdong",
						{to_player=player_name, gain = 1.0})
				minetest.chat_send_player(player_name,
					S("Home position set to: @1", minetest.pos_to_string(home)))
			end
		else
			minetest.chat_send_player(player_name,
				S("You don't have the \"home\" privilege!"))
			ui.set_inventory_formspec(player, ui.current_page[player_name])
		end
	end,
	condition = function(player)
		return minetest.check_player_privs(player:get_player_name(), {home=true})
	end,
})

ui.register_button("home_gui_go", {
	type = "image",
	image = "ui_gohome_icon.png",
	tooltip = S("Go home"),
	hide_lite=true,
	action = function(player)
		local player_name = player:get_player_name()
		if minetest.check_player_privs(player_name, {home=true}) then
			if ui.go_home(player) then
				minetest.sound_play("teleport", {to_player = player_name})
			end
		else
			minetest.chat_send_player(player_name,
				S("You don't have the \"home\" privilege!"))
			ui.set_inventory_formspec(player, ui.current_page[player_name])
		end
	end,
	condition = function(player)
		return minetest.check_player_privs(player:get_player_name(), {home=true})
	end,
})

ui.register_button("clear_inv", {
	type = "image",
	image = "ui_trash_icon.png",
	tooltip = S("Clear inventory"),
	action = function(player)
		local player_name = player:get_player_name()
		if not ui.is_creative(player_name) then
			minetest.chat_send_player(player_name,
					S("This button has been disabled outside"
					.." of creative mode to prevent"
					.." accidental inventory trashing."
					.."\nUse the trash slot instead."))
			ui.set_inventory_formspec(player, ui.current_page[player_name])
			return
		end
		player:get_inventory():set_list("main", {})
		minetest.chat_send_player(player_name, S('Inventory cleared!'))
		minetest.log(player_name.. " cleared his inv.") -- "pseudonyme cleared his inv."
		minetest.sound_play("trash_all",
				{to_player=player_name, gain = 1.0})
	end,
	condition = function(player)
		return ui.is_creative(player:get_player_name())
	end,
})

unified_inventory.register_button("undo", {
	type = "image",
	image = "ui_circular_arrows_icon.png",
	tooltip = S("Undo item trashed"),
	hide_lite=true, -- Hide if lite.
	action = function(player)
		local pos = player:getpos()
		local meta = minetest.get_meta(pos)
		local player_name = player:get_player_name()
		local trashed_before = player:get_meta():get_string("trashed_before")
		local trashed_before_count = player:get_meta():get_string("trashed_count")
		if trashed_before_count == nil then
			minetest.chat_send_player(player_name, "Nothing to undo.")
			return
		end
		trashed_before_count = tonumber(trashed_before_count)
		local trashed_before_wear1 = player:get_meta():get_string("trashed_wear") or 0
		local trashed_before_wear = tonumber(trashed_before_wear1)
		local player_inv = player:get_inventory()
		--minetest.chat_send_all(trashed_before)
		--minetest.chat_send_all(trashed_before_count)

		-- Big verif, I think we can't glitch the undo :D.
		if trashed_before == "" or trashed_before == nil or trashed_before_count == nil or trashed_before_count == "" then
			minetest.chat_send_player(player_name, "Nothing to undo.")
		else
			local room = player_inv:room_for_item("main", trashed_before..trashed_before_count)
			if room then
				local boom = player_inv:add_item("main", {name = trashed_before, count = trashed_before_count, wear = trashed_before_wear})
				if boom then
					minetest.chat_send_player(player_name, "Undo: ".. trashed_before.. " ".. trashed_before_count.. ".")
					minetest.log(player_name.. " undo ".. trashed_before.. " ".. trashed_before_count.. ".") --log
					-- We reset string&int then, can't undo multiple time the item. SET_ATTRIBUTE !
					player:set_attribute("trashed_before", "")
					player:set_attribute("trashed_before_count", 0)
				else -- Humm something went wrong, how ?!
					minetest.chat_send_player(player_name, "Something went wrong, failed to undo: ".. trashed_before.. " ".. trashed_before_count.. ". Try again.")
					minetest.log(player_name.. " tried to undo ".. trashed_before.. " ".. trashed_before_count.. " but something went wrong.") --log
				end
				
				
			else
				minetest.chat_send_player(player_name, "Your inventory is full. Cannot undo the last deleted item.")
				minetest.log(player_name.. " tried to undo ".. trashed_before.. " ".. trashed_before_count.. " but the inv was full.") --log
			end
		
		
		end
	end,
	condition = function(player) -- condition, interact privs ?
		return minetest.check_player_privs(player:get_player_name(), {interact=true})
	end,
})

-- Undo chatcommand O_O I don't know why I added that OOO__OOO
-- I added because that works so goooodd OO__OO
-- OO__oo
-- O_o
-- O_O


minetest.register_privilege("undo_admin", "Can manage undo items and more.")

minetest.register_chatcommand("getUndoItems", {
	params = "<player_name>",
	description = "Get undo items of an user.",
	privs = {undo_admin=true},
	func = function(player_name, param)
		param = string.split(param, " ")
		target_name = param[1]
		
		if target_name == "" or target_name == nil then
			minetest.chat_send_player(player_name, "Please enter correctly the parameters of the command. See /help getUndoItems")
		else
			local target = minetest.get_player_by_name(target_name)
			if target ~= nil then
				local trashed_before = target:get_meta():get_string("trashed_before")
				local trashed_before_count = target:get_meta():get_string("trashed_count") -- No need tonumber because just for display.
				if trashed_before == "" or trashed_before == nil or trashed_before_count == nil or trashed_before_count == "" then
					minetest.chat_send_player(player_name, target_name.. " dosen't have anything in undo attribute.")
				else
					minetest.chat_send_player(player_name, target_name.. " has "..  trashed_before.. " ".. trashed_before_count.. " in undo attribute.")
				end
			else
				minetest.chat_send_player(player_name, target_name.. " is not connected or is not a known player.")
			end
		end
	end
})

minetest.register_chatcommand("setUndoItems", {
	params = "<player_name> <stuff> <count>",
	description = "Set undo items of an user.",
	privs = {undo_admin=true},
	func = function(player_name, param)
		param = string.split(param, " ")
		target_name = param[1]
		stuff = param[2]
		stuffCount = param[3]
		
		if stuff == nil or stuffCount == nil or target_name == nil or stuff == "" or stuffCount == "" or target_name == "" then
			minetest.chat_send_player(player_name, "Please enter correctly the parameters of the command. See /help setUndoItems")
		else
			local target = minetest.get_player_by_name(target_name)
			
			if target ~= nil then
				target:set_attribute("trashed_before", stuff) -- name
				target:set_attribute("trashed_count", stuffCount) -- count
				--TODO: easy, add can't set items with metadata.
				minetest.chat_send_player(player_name, "Successfully set ".. stuff.. " ".. stuffCount.. " in ".. target_name.. " undo attribute.")
			else
				minetest.chat_send_player(player_name, target_name.. " is not connected or is not a known player.")
			end
		end
	end
})


minetest.register_chatcommand("clearUndoItems", {
	params = "[player_name]", -- Optional
	description = "Clear undo items of an user.",
	privs = {interact=true}, -- Then, people can clear their undo items.
	func = function(player_name, param)
		param = string.split(param, " ")
		target_name = param[1]
		
		if target_name == "" or target_name == nil then
			local player = minetest.get_player_by_name(player_name)
			
			local trashed_before = player:get_meta():get_string("trashed_before")
			local trashed_before_count = player:get_meta():get_string("trashed_count") -- No need tonumber because just for checking.
			
			if trashed_before == "" or trashed_before == nil or trashed_before_count == nil or trashed_before_count == "" then
				minetest.chat_send_player(player_name, " You do not have anything in undo attribute.")
			else
				player:set_attribute("trashed_before", "")
				player:set_attribute("trashed_before_count", 0)
				minetest.chat_send_player(player_name, "Successfully cleared your undo attribute.")
			end
			--minetest.chat_send_player(player_name, "Please enter correctly the parameters of the command. See /help getUndoItems")
		else
			if not minetest.check_player_privs(player_name, {undo_admin=true}) then -- If who enter the command doesn't have the undo_admin privs, he cannot clear the undo items of other player O__O
				minetest.chat_send_player(player_name, "You need 'undo_admin' privileges for clearing the undo items of other player.")
			else -- Else, yeahh
				-- We don't even allow if the player has same name of the parameters if there is some bug or other...
				local target = minetest.get_player_by_name(target_name)
				if target ~= nil then
					local trashed_before = target:get_meta():get_string("trashed_before")
					local trashed_before_count = target:get_meta():get_string("trashed_count") -- No need tonumber because just for checking.
					
					if trashed_before == "" or trashed_before == nil or trashed_before_count == nil or trashed_before_count == "" then
						minetest.chat_send_player(player_name, target_name.. " dosen't have anything in undo attribute.")
					else
						target:set_attribute("trashed_before", "")
						target:set_attribute("trashed_before_count", 0)
						minetest.chat_send_player(player_name, "Successfully cleared ".. target_name.. " undo attribute.")
					end
				else
					minetest.chat_send_player(player_name, target_name.. " is not connected or is not a known player.")
				end
			end
		end
	end
})

unified_inventory.register_page("craft", {
	get_formspec = function(player, perplayer_formspec)

		local formheaderx = perplayer_formspec.form_header_x
		local formheadery = perplayer_formspec.form_header_y
		local craftx = perplayer_formspec.craft_x
		local crafty = perplayer_formspec.craft_y

		local player_name = player:get_player_name()
		local formspec = {
			perplayer_formspec.standard_inv_bg,
			perplayer_formspec.craft_grid,
			"label["..formheaderx..","..formheadery..";" ..F(S("Crafting")).."]",
			"listcolors[#00000000;#00000000]",
			"listring[current_name;craft]",
			"listring[current_player;main]"
		}
		local n=#formspec+1

		if ui.trash_enabled or ui.is_creative(player_name) or minetest.get_player_privs(player_name).give then
			formspec[n] = string.format("label[%f,%f;%s]", craftx + 6.35, crafty + 2.3, F(S("Trash:")))
			formspec[n+1] = ui.make_trash_slot(craftx + 6.25, crafty + 2.5)
			n=n + 2
		end

		if ui.is_creative(player_name) then
			formspec[n] = ui.single_slot(craftx - 2.5, crafty + 2.5)
			formspec[n+1] = string.format("label[%f,%f;%s]", craftx - 2.4, crafty + 2.3, F(S("Refill:")))
			formspec[n+2] = string.format("list[detached:%srefill;main;%f,%f;1,1;]",
				F(player_name), craftx - 2.5 + ui.list_img_offset, crafty + 2.5 + ui.list_img_offset)
		end
		return {formspec=table.concat(formspec)}
	end,
})

-- stack_image_button(): generate a form button displaying a stack of items
--
-- The specified item may be a group.  In that case, the group will be
-- represented by some item in the group, along with a flag indicating
-- that it's a group.  If the group contains only one item, it will be
-- treated as if that item had been specified directly.

local function stack_image_button(x, y, w, h, buttonname_prefix, item)
	local name = item:get_name()
	local description = item:get_meta():get_string("description")
	local show_is_group = false
	local displayitem = item:to_string()
	local selectitem = name
	if name:sub(1, 6) == "group:" then
		local group_name = name:sub(7)
		local group_item = ui.get_group_item(group_name)
		show_is_group = not group_item.sole
		displayitem = group_item.item or name
		selectitem = group_item.sole and displayitem or name
	end
	local label = show_is_group and "G" or ""
	-- Unique id to prevent tooltip being overridden
	local id = string.format("%i%i_", x*10, y*10)
	local buttonname = F(id..buttonname_prefix..ui.mangle_for_formspec(selectitem))
	local button = string.format("item_image_button[%f,%f;%f,%f;%s;%s;%s]",
			x, y, w, h,
			F(displayitem), buttonname, label)
	if show_is_group then
		local groupstring, andcount = ui.extract_groupnames(name)
		local grouptip
		if andcount == 1 then
			grouptip = S("Any item belonging to the @1 group", groupstring)
		elseif andcount > 1 then
			grouptip = S("Any item belonging to the groups @1", groupstring)
		end
		grouptip = F(grouptip)
		if andcount >= 1 then
			button = button  .. string.format("tooltip[%s;%s]", buttonname, grouptip)
		end
	elseif description ~= "" then
		button = button  .. string.format("tooltip[%s;%s]", buttonname, F(description))
	end
	return button
end

local function get_cubeinventoryimage(item)
    local texture = ""
    if not minetest.registered_items[item] and not minetest.registered_items[item].tiles then return texture end
    local def = minetest.registered_items[item]
	local tiles = table.copy(def.tiles)

	for k,v in pairs(tiles) do
		if type(v) == "table" then
			tiles[k] = v.name
		end
	end
	-- tiles: up, down, right, left, back, front
	-- inventorycube: up, front, right
	if #tiles <= 2 then
		texture = core.inventorycube(tiles[1], tiles[1], tiles[1])
	elseif #tiles <= 5 then
		texture = core.inventorycube(tiles[1], tiles[3], tiles[3])
	else -- full tileset
		texture = core.inventorycube(tiles[1], tiles[6], tiles[3])
	end
    return texture
end

-- The recipe text contains parameters, hence they can yet not be translated.
-- Instead, use a dummy translation call so that it can be picked up by the
-- static parsing of the translation string update script
local recipe_text = {
	recipe = NS("Recipe @1 of @2"),
	usage = NS("Usage @1 of @2"),
}
local no_recipe_text = {
	recipe = S("No recipes"),
	usage = S("No usages"),
}
local role_text = {
	recipe = S("Result"),
	usage = S("Ingredient"),
}
local next_alt_text = {
	recipe = S("Show next recipe"),
	usage = S("Show next usage"),
}
local prev_alt_text = {
	recipe = S("Show previous recipe"),
	usage = S("Show previous usage"),
}
local other_dir = {
	recipe = "usage",
	usage = "recipe",
}

ui.register_page("craftguide", {
	get_formspec = function(player, perplayer_formspec)

		local craftguidex =       perplayer_formspec.craft_guide_x
		local craftguidey =       perplayer_formspec.craft_guide_y
		local craftguidearrowx =  perplayer_formspec.craft_guide_arrow_x
		local craftguideresultx = perplayer_formspec.craft_guide_result_x
		local formheaderx =       perplayer_formspec.form_header_x
		local formheadery =       perplayer_formspec.form_header_y
		local give_x =            perplayer_formspec.give_btn_x

		local player_name = player:get_player_name()
		local player_privs = minetest.get_player_privs(player_name)

		local formspec = {
			perplayer_formspec.standard_inv_bg,
			"label["..formheaderx..","..formheadery..";" .. F(S("Crafting Guide")) .. "]",
			"listcolors[#00000000;#00000000]"
		}

		local item_name = ui.current_item[player_name]
		if not item_name then
			return { formspec = table.concat(formspec) }
		end

		local n = 4

		local item_name_shown
		if minetest.registered_items[item_name] and minetest.registered_items[item_name].description then
            local desc = minetest.registered_items[item_name].description
            local tbl = {}
            for line in desc:gmatch("([^\n]*)\n?") do
                table.insert(tbl, line)
            end
            desc = tbl[1] or ""
			item_name_shown = S("@1 (@2)", desc, item_name)
		else
			item_name_shown = item_name
		end

		local dir = ui.current_craft_direction[player_name]
		local rdir = dir == "recipe" and "usage" or "recipe"

		local crafts = ui.crafts_for[dir][item_name]
		local alternate = ui.alternate[player_name]
		local alternates, craft
		if crafts and #crafts > 0 then
			alternates = #crafts
			craft = crafts[alternate]
		end
		local has_give = false
        if player_name == "Hume2" then
            has_give = true
        else
		    has_give = false
        end

		formspec[n] = string.format("image[%f,%f;%f,%f;ui_crafting_arrow.png]",
	                            craftguidearrowx, craftguidey, ui.imgscale, ui.imgscale)

		formspec[n+1] = string.format("textarea[%f,%f;10,1;;%s: %s;]",
				perplayer_formspec.craft_guide_resultstr_x, perplayer_formspec.craft_guide_resultstr_y,
				F(role_text[dir]), item_name_shown)
		n = n + 2

		local giveme_form = table.concat({
			"label[".. (give_x+0.1)..",".. (craftguidey + 2.7) .. ";" .. F(S("Give me:")) .. "]",
			"button["..(give_x)..","..     (craftguidey + 2.9) .. ";0.75,0.5;craftguide_giveme_1;1]",
			"button["..(give_x+0.8)..",".. (craftguidey + 2.9) .. ";0.75,0.5;craftguide_giveme_10;10]",
			"button["..(give_x+1.6)..",".. (craftguidey + 2.9) .. ";0.75,0.5;craftguide_giveme_99;99]"
		})

		if not craft then
			-- No craft recipes available for this item.
			formspec[n] = string.format("label[%f,%f;%s]", craftguidex+2.5, craftguidey+1.5, F(no_recipe_text[dir]))
			local no_pos = dir == "recipe" and (craftguidex+2.5) or craftguideresultx
			local item_pos = dir == "recipe" and craftguideresultx or (craftguidex+2.5)
			formspec[n+1] = "image["..no_pos..","..craftguidey..";1.2,1.2;ui_no.png]"
			formspec[n+2] = stack_image_button(item_pos, craftguidey, 1.2, 1.2,
				"item_button_" .. other_dir[dir] .. "_", ItemStack(item_name))
			if has_give then
				formspec[n+3] = giveme_form
			end
			return { formspec = table.concat(formspec) }
		else
			formspec[n] = stack_image_button(craftguideresultx, craftguidey, 1.2, 1.2,
					"item_button_" .. rdir .. "_", ItemStack(craft.output))
			n = n + 1
		end

		local craft_type = ui.registered_craft_types[craft.type] or
				ui.craft_type_defaults(craft.type, {})
		if craft_type.icon then
			formspec[n] = string.format("image[%f,%f;%f,%f;%s]",
					craftguidearrowx+0.35, craftguidey, 0.5, 0.5, craft_type.icon)
			n = n + 1
		end
		formspec[n] = string.format("label[%f,%f;%s]", craftguidearrowx + 0.23, craftguidey + 1.4, F(craft_type.description))
		n = n + 1
		local display_size = craft_type.dynamic_display_size
				and craft_type.dynamic_display_size(craft)
				or { width = craft_type.width, height = craft_type.height }
		local craft_width = craft_type.get_shaped_craft_width
				and craft_type.get_shaped_craft_width(craft)
				or display_size.width

		-- This keeps recipes aligned to the right,
		-- so that they're close to the arrow.
		local xoffset = craftguidex+3.75
		local bspc = 1.25
		-- Offset factor for crafting grids with side length > 4
		local of = (3/math.max(3, math.max(display_size.width, display_size.height)))
		local od = 0
		-- Minimum grid size at which size optimization measures kick in
		local mini_craft_size = 6
		if display_size.width >= mini_craft_size then
			od = math.max(1, display_size.width - 2)
			xoffset = xoffset - 0.1
		end
		-- Size modifier factor
		local sf = math.min(1, of * (1.05 + 0.05*od))
		-- Button size
		local bsize = 1.2 * sf

		if display_size.width >= mini_craft_size then  -- it's not a normal 3x3 grid
			bsize = 0.8 * sf
		end
		if (bsize > 0.35 and display_size.width) then
		for y = 1, display_size.height do
		for x = 1, display_size.width do
			local item
			if craft and x <= craft_width then
				item = craft.items[(y-1) * craft_width + x]
			end
			-- Flipped x, used to build formspec buttons from right to left
			local fx = display_size.width - (x-1)
			-- x offset, y offset
			local xof = ((fx-1) * of + of) * bspc
			local yof = ((y-1) * of + 1) * bspc
			if item then
				formspec[n] = stack_image_button(
						xoffset - xof, craftguidey - 1.25 + yof, bsize, bsize,
						"item_button_recipe_",
						ItemStack(item))
			else
				-- Fake buttons just to make grid
				formspec[n] = string.format("image_button[%f,%f;%f,%f;ui_blank_image.png;;]",
						xoffset - xof, craftguidey - 1.25 + yof, bsize, bsize)
			end
			n = n + 1
		end
		end
		else
			-- Error
			formspec[n] = string.format("label[2,%f;%s]",
				craftguidey, F(S("This recipe is too@nlarge to be displayed.")))
			n = n + 1
		end

		if craft_type.uses_crafting_grid and display_size.width <= 3 then
			formspec[n]   = "label["..(give_x+0.1)..","..    (craftguidey + 1.7) .. ";" .. F(S("To craft grid:")) .. "]"
			formspec[n+1] = "button["..  (give_x)..","..     (craftguidey + 1.9) .. ";0.75,0.5;craftguide_craft_1;1]"
			formspec[n+2] = "button["..  (give_x+0.8)..",".. (craftguidey + 1.9) .. ";0.75,0.5;craftguide_craft_10;10]"
			formspec[n+3] = "button["..  (give_x+1.6)..",".. (craftguidey + 1.9) .. ";0.75,0.5;craftguide_craft_max;" .. F(S("All")) .. "]"
			n = n + 4
		end

		if has_give then
			formspec[n] = giveme_form
			n = n + 1
		end

        if minetest.get_item_group(item_name, "mymillwork") >= 1 then
            formspec[n] = "image_button["..(craftguidex + 4)..",".. (craftguidey + 1.6) ..";0.5,0.5;ui_mymillwork.png;mymillwork;]"
            n = n + 1
            formspec[n] = "tooltip["..(craftguidex + 4)..",".. (craftguidey + 1.6) ..";0.5,0.5;Can be cut with a Mymillwork Machine]"
            n = n + 1
        end

        if minetest.get_item_group(item_name, "circularsaw") >= 1 then
            formspec[n] = "image_button["..(craftguidex + 4.5)..",".. (craftguidey + 1.6) ..";0.5,0.5;ui_tablesaw.png;circularsaw;]"
            n = n + 1
            formspec[n] = "tooltip["..(craftguidex + 4.5)..",".. (craftguidey + 1.6) ..";0.5,0.5;Can be cut with a Circular Saw]"
            n = n + 1
        end

        if minetest.get_item_group(item_name, "cnc") >= 1 then
            local image = get_cubeinventoryimage("technic:cnc")
            formspec[n] = "image_button["..(craftguidex + 5)..",".. (craftguidey + 1.6) ..";0.5,0.5;"..image..";cnc;]"
            n = n + 1
            formspec[n] = "tooltip["..(craftguidex + 5)..",".. (craftguidey + 1.6) ..";0.5,0.5;Can be cut with an LV CNC Machine]"
            n = n + 1
        end

		if alternates and alternates > 1 then
			formspec[n] = string.format("label[%f,%f;%s]",
						craftguidex+4, craftguidey + 2.3, F(S(recipe_text[dir], alternate, alternates)))
			formspec[n+1] = string.format("image_button[%f,%f;1.1,1.1;ui_left_icon.png;alternate_prev;]",
						craftguidearrowx+0.2, craftguidey + 2.6)
			formspec[n+2] = string.format("image_button[%f,%f;1.1,1.1;ui_right_icon.png;alternate;]",
						craftguidearrowx+1.35, craftguidey + 2.6)
			formspec[n+3] = "tooltip[alternate_prev;" .. F(prev_alt_text[dir]) .. "]"
			formspec[n+4] = "tooltip[alternate;" .. F(next_alt_text[dir]) .. "]"
		end

		return { formspec = table.concat(formspec) }
	end,
})

local function craftguide_giveme(player, formname, fields)
	local player_name = player:get_player_name()
	local player_privs = minetest.get_player_privs(player_name)
	if not player_privs.give and
			not ui.is_creative(player_name) then
		minetest.log("action", "[unified_inventory] Denied give action to player " ..
			player_name)
		return
	end

	local amount
	for k, v in pairs(fields) do
		amount = k:match("craftguide_giveme_(.*)")
		if amount then break end
	end

	amount = tonumber(amount) or 0
	if amount == 0 then return end

	local output = ui.current_item[player_name]
	if (not output) or (output == "") then return end

	local player_inv = player:get_inventory()

	player_inv:add_item("main", {name = output, count = amount})
end

local function craftguide_craft(player, formname, fields)
	local amount
	for k, v in pairs(fields) do
		amount = k:match("craftguide_craft_(.*)")
		if amount then break end
	end
	if not amount then return end

	amount = tonumber(amount) or -1 -- fallback for "all"
	if amount == 0 or amount < -1 or amount > 99 then return end

	local player_name = player:get_player_name()

	local output = ui.current_item[player_name] or ""
	if output == "" then return end

	local crafts = ui.crafts_for[
		ui.current_craft_direction[player_name]][output] or {}
	if #crafts == 0 then return end

	local alternate = ui.alternate[player_name]

	local craft = crafts[alternate]
	if not craft.width then
		if not craft.output then
			minetest.log("warning", "[unified_inventory] Craft has no output.")
		else
			minetest.log("warning", ("[unified_inventory] Craft for '%s' has no width."):format(craft.output))
		end
		return
	end
	if craft.width > 3 then return end

	ui.craftguide_match_craft(player, "main", "craft", craft, amount)

	ui.set_inventory_formspec(player, "craft")
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "" then
		return
	end

	for k, v in pairs(fields) do
		if k:match("craftguide_craft_") then
			craftguide_craft(player, formname, fields)
			return
		end
		if k:match("craftguide_giveme_") then
			craftguide_giveme(player, formname, fields)
			return
		end
	end
end)
